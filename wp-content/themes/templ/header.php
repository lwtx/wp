<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Required meta tags -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">

    <title><?php bloginfo('name'); ?> | <?php the_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body>

<nav class="navbar fixed-top navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="/">
        <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="" class="img-fluid logo">
    </a>
    <span></span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php wp_nav_menu( array(
            'theme_location' => 'menu',
            'container' => '',
            'menu_class' => 'navbar-nav mr-auto',
            'items_wrap' => '<ul class="%2$s"><li class="nav-item"><a class="nav-link">%3$s</a></li></ul>',
        ) ); ?>
    </div>
</nav>