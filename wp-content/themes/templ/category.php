<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col section-content">
            <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>

                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                <?php the_tags('Категория: ') ?>
                <?php the_excerpt() ?>
                <a href="<?php the_permalink() ?>">Читать далее...</a>

            <?php endwhile; ?>
            <?php endif; ?>
            <div class="clear"></div>
            <br>
            <?php /*posts_nav_link('<<', __('Новые записи >'), __('< Предыдущие записи')) */?>
            <?php wp_pagenavi(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
