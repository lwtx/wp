<?php
/*
Template Name: Home Template
*/
?>
<?php get_header(); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col section-1">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="/tag/interiors/"><div class="section-1-type-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/images/section-1-type-img-1.jpg" alt="" class="img-fluid section-1-type-img-1">
                                        <div class="section-1-type-img-line-1">
                                            <p>Интерьеры</p>
                                        </div>
                                    </div></a>
                            </div>
                            <div class="col-sm-6">
                                <a href="/tag/exteriors/"><div class="section-1-type-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/images/section-1-type-img-2.jpg" alt="" class="img-fluid section-1-type-img-1">
                                        <div class="section-1-type-img-line-1">
                                            <p>Экстерьеры</p>
                                        </div>
                                    </div></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="/tag/subviz/"><div class="section-1-type-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/images/section-1-type-img-3.jpg" alt="" class="img-fluid section-1-type-img-1">
                                        <div class="section-1-type-img-line-1">
                                            <p>Предметная визуализация</p>
                                        </div>
                                    </div></a>
                            </div>
                            <div class="col-sm-6">
                                <a href="/tag/advviz/"><div class="section-1-type-img-block">
                                        <img src="<?php bloginfo('template_url'); ?>/images/section-1-type-img-4.jpg" alt="" class="img-fluid section-1-type-img-1">
                                        <div class="section-1-type-img-line-1">
                                            <p>Рекламная визуализация</p>
                                        </div>
                                    </div></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

