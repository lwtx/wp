<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col section-content">
            <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>

                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                <?php the_tags('Категория: ') ?>
                <?php the_excerpt() ?>
                <div class="clear"></div>
                <a href="<?php the_permalink() ?>">Подробнее...</a>

            <?php endwhile; ?>
            <?php endif; ?>
            <div class="clear"></div>
            <br>
            <?php posts_nav_link('<<', __('< Предыдущие записи'), __('Новые записи >')) ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
