<?php
/*
Plugin Name: Templ-Options-Page
Description: Template Options Page
Plugin URI: http://templ.com
Author: Templ
Author URI: http://templ.com
*/

add_action( 'admin_menu', 'templ_admin_menu' );
add_action( 'admin_init', 'templ_admin_settings' );

function templ_admin_settings(){
	// $option_group, $option_name, $sanitize_callback
	register_setting( 'templ_theme_options_group', 'templ_theme_options', 'templ_theme_options_sanitize' );

	// $id, $title, $callback, $page
	add_settings_section( 'templ_theme_options_id', 'Секция Опции темы', '', 'templ-theme-options' );
	// $id, $title, $callback, $page, $section, $args
	add_settings_field( 'templ_theme_options_title', 'Цвет заголовка', 'templ_theme_title_cb', 'templ-theme-options', 'templ_theme_options_id' , array('label_for' => 'templ_theme_options_title') );
	add_settings_field( 'templ_theme_options_text', 'Цвет текста', 'templ_theme_text_cb', 'templ-theme-options', 'templ_theme_options_id', array('label_for' => 'templ_theme_options_text') );
}

function templ_theme_title_cb(){
	$options = get_option('templ_theme_options');
	?>

<input type="text" name="templ_theme_options[templ_theme_options_title]" id="templ_theme_options_title" value="<?php echo esc_attr($options['templ_theme_options_title']); ?>" class="regular-text">

	<?php
}

function templ_theme_text_cb(){
	$options = get_option('templ_theme_options');
	?>

<input type="text" name="templ_theme_options[templ_theme_options_text]" id="templ_theme_options_text" value="<?php echo esc_attr($options['templ_theme_options_text']); ?>" class="regular-text">

	<?php
}

function templ_theme_options_sanitize($options){
	$clean_options = array();
	foreach($options as $k => $v){
		$clean_options[$k] = strip_tags($v);
	}
	return $clean_options;
}

function templ_admin_menu(){
	// $page_title, $menu_title, $capability, $menu_slug, $function
	add_options_page( 'Опции темы (title)', 'Опции темы', 'manage_options', 'templ-theme-options', 'templ_option_page' );
}

function templ_option_page(){
	$options = get_option( 'templ_theme_options' );
	?>

<div class="wrap">
	<h2>Опции темы</h2>
	<p>Настройки темы плагина</p>
	<form action="options.php" method="post">
		<?php settings_fields( 'templ_theme_options_group' ); ?>
		<?php do_settings_sections( 'templ-theme-options' ); ?>
		<?php submit_button(); ?>
	</form>
</div>

	<?php
}