<?php
/*
Plugin Name: Templ-Options-Menu
Description: Template Options Menu
Plugin URI: http://templ.com
Author: Templ
Author URI: http://templ.com
*/

add_action( 'admin_menu', 'templ_admin_menu' );
add_action( 'admin_init', 'templ_admin_settings' );

function templ_admin_menu(){
	// $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position
	add_menu_page( 'Опции темы (title)', 'Опции темы', 'manage_options', __FILE__, 'templ_option_page', plugins_url( 'templ-ico.png', __FILE__ ) );
}

function templ_admin_settings(){
	// $option_group, $option_name, $sanitize_callback
	register_setting( 'templ_group', 'templ_theme_options', 'templ_theme_options_sanitize' );

	// $id, $title, $callback, $page
	add_settings_section( 'templ_section_title_id', 'Секция TITLE Опции темы', '', __FILE__ );
	add_settings_section( 'templ_section_text_id', 'Секция TEXT Опции темы', '', __FILE__ );

	// $id, $title, $callback, $page, $section, $args
	add_settings_field( 'templ_setting_title_id', 'Цвет заголовка', 'templ_theme_title_cb', __FILE__, 'templ_section_title_id' , array('label_for' => 'templ_setting_title_id') );
	add_settings_field( 'wfm_setting_title_id2', 'Размер заголовка', 'templ_theme_title_cb2', __FILE__, 'templ_section_title_id' , array('label_for' => 'templ_setting_title_id2') );

	add_settings_field( 'templ_setting_text_id', 'Цвет текста', 'templ_theme_text_cb', __FILE__, 'templ_section_text_id', array('label_for' => 'templ_setting_text_id') );
}

function templ_theme_title_cb(){
	$options = get_option('templ_theme_options');
	?>

<input type="text" name="templ_theme_options[title]" id="templ_setting_title_id" value="<?php echo esc_attr($options['title']); ?>" class="regular-text">

	<?php
}

function templ_theme_title_cb2(){
	$options = get_option('templ_theme_options');
	?>

<input type="text" name="templ_theme_options[size]" id="templ_setting_title_id2" value="<?php echo esc_attr($options['size']); ?>" class="regular-text">

	<?php
}

function templ_theme_text_cb(){
	$options = get_option('templ_theme_options');
	?>

<input type="text" name="templ_theme_options[text]" id="templ_setting_text_id" value="<?php echo esc_attr($options['text']); ?>" class="regular-text">

	<?php
}

function templ_option_page(){
	?>

<div class="wrap">
	<h2>Опции темы</h2>
	<p>Настройки темы плагина Options &amp; Settings API</p>
	<form action="options.php" method="post">
		<?php settings_fields( 'templ_group' ); ?>
		<?php do_settings_sections( __FILE__ ); ?>
		<?php submit_button(); ?>
	</form>
</div>

	<?php
}

function templ_theme_options_sanitize($options){
	$clean_options = array();
	foreach($options as $k => $v){
		$clean_options[$k] = strip_tags($v);
	}
	return $clean_options;
}