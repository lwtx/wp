<?php
/*
Plugin Name: Templ Shortcode
Description: Template Shortcode
Plugin URI: http://templ.com
Author: Templ
Author URI: http://templ.com
*/

add_shortcode( 'portf', 'templ_portf' );

function templ_portf($atts, $content){
    $atts = shortcode_atts(
        array(
            'base' => 'В базе',
            'count' => '12',
            'content' => !empty($content) ? $content : 'Список портфолио:'
        ), $atts
    );
    extract($atts);
    return "<h3>{$content}</h3><p>{$base} присутствуют {$count} работ</p>";
}
//[portf base="В базе" count="10"][/portf]