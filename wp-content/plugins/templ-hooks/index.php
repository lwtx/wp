<?php
/*
Plugin Name: Хуки
Description: Хуки
Plugin URI: http://templ.com
Author: Templ
Author URI: http://templ.com
*/


/*add_filter( 'the_title', 'templ_title' );

function templ_title($title){
	if( is_admin() ) return $title;
	return mb_convert_case($title, MB_CASE_TITLE, "UTF-8");
}*/

// add_filter( 'the_title', 'ucwords' );

add_filter( 'the_content', 'templ_content' );

function templ_content($content){
	if( is_user_logged_in() ) return $content;
	if( is_page() ) return $content;
	return '<div class="templ-access"><a href="' . home_url() . '/wp-login.php">Авторизуйтесь для просмотра контента</a></div>';
}

/*add_action( 'comment_post', 'templ_comment_post' );

function templ_comment_post(){
	wp_mail( get_bloginfo( 'admin_email' ), 'Новый комментарий на сайте', 'На сайте появился новый комментарий' );
}*/