<?php
/*
Plugin Name: Templ Widget
Description: Template Widget
Plugin URI: http://templ.com
Author: Templ
Author URI: http://templ.com
*/

add_action( 'widgets_init', 'templ_first_widget' );

function templ_first_widget(){
	register_widget( 'TEMPL_Widget' );
}

class TEMPL_Widget extends WP_Widget{

	function __construct(){
		/*parent::__construct(
			// ID, name, args (description)
			'templ_fw',
			'Виджет Templ',
			array( 'description' => 'Описание виджета' )
		);*/
		$args = array(
			'name' => 'Виджет Templ',
			'description' => 'Виджет Templ',
			'classname' => 'templ-wid'
		);
		parent::__construct('templ_fw', '', $args);
	}

	function widget($args, $instance){
		extract($args);
		extract($instance);

		$title = apply_filters( 'widget_title', $title );
		$text = apply_filters( 'widget_text', $text );

		echo $before_widget;
		echo $before_title . $title . $after_title;
		echo "<div>$text</div>";
		echo $after_widget;
	}

	function form($instance){
		extract($instance);
		?>
		
		<p>
			<label for="<?php echo $this->get_field_id('title') ?>">Заголовок:</label>
			<input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?>" value="<?php if( isset($title) ) echo esc_attr( $title ); ?>" class="widefat">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('text') ?>">Текст:</label>
			<textarea class="widefat" name="<?php echo $this->get_field_name('text') ?>" id="<?php echo $this->get_field_id('text') ?>" cols="20" rows="5"><?php if( isset($text) ) echo esc_attr( $text ); ?></textarea>
		</p>

		<?php
	}

	function update($new_instance, $old_instance){
		$new_instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
		$new_instance['text'] = str_replace('<p>', '', $new_instance['text']);
		$new_instance['text'] = str_replace('</p>', '<br>', $new_instance['text']);
		return $new_instance;
	}

}