<?php
/*
Plugin Name: Templ-Options
Description: Template options
Plugin URI: http://templ.com
Author: Templ
Author URI: http://templ.com
*/

add_action( 'admin_init', 'templ_theme_options' );

function templ_theme_options(){
	register_setting( 'general', 'templ_theme_options' );

	// $id - ID секции
	// $title - заголовок
	// $callback - callback-функция для генерации HTML-кода
	// $page - для какой страницы регистрируется секция
	add_settings_section( 'templ_theme_section_id', 'Опции темы', 'templ_theme_options_section_cb', 'general' );

	add_settings_field( 'templ_theme_options_title', 'Цвет заголовка', 'templ_theme_body_cb', 'general', 'templ_theme_section_id' );
	add_settings_field( 'templ_theme_options_text', 'Цвет текста', 'templ_theme_header_cb', 'general', 'templ_theme_section_id' );
}

function templ_theme_options_section_cb(){
	echo '<p>Template Options</p>';
}

function templ_theme_body_cb(){
	$options = get_option('templ_theme_options');
	?>

<input type="text" name="templ_theme_options[templ_theme_options_title]" id="templ_theme_options_title" value="<?php echo esc_attr($options['templ_theme_options_title']); ?>" class="regular-text">

	<?php
}

function templ_theme_header_cb(){
	$options = get_option('templ_theme_options');
	?>

<input type="text" name="templ_theme_options[templ_theme_options_text]" id="templ_theme_options_text" value="<?php echo esc_attr($options['templ_theme_options_text']); ?>" class="regular-text">

	<?php
}