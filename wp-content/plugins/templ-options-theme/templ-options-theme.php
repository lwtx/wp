<?php
/*
Plugin Name: Templ-Options-Theme
Description: Создание настраиваемой темы
Plugin URI: http://templ.com
Author: Templ
Author URI: http://templ.com
*/

add_action( 'admin_menu', 'templ_options_page' );
add_action( 'admin_init', 'templ_setting' );

function templ_options_page(){
	// $page_title, $menu_title, $capability, $menu_slug, $function
	add_options_page( 'Опции темы', 'Опции темы', 'manage_options', 'templ-options-theme', 'templ_option_page' );
}

function templ_setting(){
	// $option_group, $option_name, $sanitize_callback
	register_setting( 'templ_options_group', 'templ_options_theme', 'templ_options_sanitize' );

	// $id, $title, $callback, $page
	add_settings_section( 'templ_options_section', 'Опции темы', '', 'templ-options-theme' );

	// $id, $title, $callback, $page, $section, $args
	add_settings_field( 'templ_body_bg_id', 'Цвет заголовка', 'templ_body_bg_cb', 'templ-options-theme', 'templ_options_section', array( 'label_for' => 'templ_body_bg_id' ) );
	add_settings_field( 'templ_header_pic_id', 'Картинка в хедере', 'templ_header_pic_cb', 'templ-options-theme', 'templ_options_section', array( 'label_for' => 'templ_header_pic_id' ) );
	add_settings_field( 'templ_check_pic_id', 'Удалить картинку', 'templ_check_pic_cb', 'templ-options-theme', 'templ_options_section', array( 'label_for' => 'templ_check_pic_id' ) );
}

function templ_body_bg_cb(){
	$options = get_option( 'templ_options_theme' );
	?>
<input type="text" name="templ_options_theme[body_bg]" id="templ_body_bg_id" value="<?php echo esc_attr($options['body_bg']); ?>" class="regular-text">
	<?php
}

function templ_header_pic_cb(){
	?>
<input type="file" name="templ_options_theme_file" id="templ_header_pic_id">
	<?php
	$options = get_option( 'templ_options_theme' );
	if( !empty($options['file']) ){
		echo "<p><img src='{$options['file']}' alt='' width='200'></p>";
	}
}

function templ_check_pic_cb(){
	?>
<input type="checkbox" name="templ_options_theme_file_check" id="templ_check_pic_id">
	<?php
}

function templ_options_sanitize($options){
	if( !empty($_FILES['templ_options_theme_file']['tmp_name']) ){
		$overrides = array('test_form' => false);
		$file = wp_handle_upload( $_FILES['templ_options_theme_file'], $overrides );
		$options['file'] = $file['url'];
	}else{
		$old_oprions = get_option( 'templ_options_theme' );
		$options['file'] = $old_oprions['file'];
	}

	if( isset($_POST['templ_options_theme_file_check']) && $_POST['templ_options_theme_file_check'] == 'on' ){
		unset($options['file']);
	}
	$clean_options = array();
	foreach($options as $k => $v){
		$clean_options[$k] = strip_tags($v);
	}
	return $clean_options;
}

function templ_option_page(){
	?>
<div class="wrap">
	<h2>Опции темы</h2>
	<form action="options.php" method="post" enctype="multipart/form-data">
		<?php settings_fields( 'templ_options_group' ); ?>
		<?php do_settings_sections( 'templ-options-theme' ); ?>
		<?php submit_button(); ?>
	</form>
</div>
	<?php
}