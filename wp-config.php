<?php
/**
* Основные параметры WordPress.
*
* Скрипт для создания wp-config.php использует этот файл в процессе
* установки. Необязательно использовать веб-интерфейс, можно
* скопировать файл в "wp-config.php" и заполнить значения вручную.
*
* Этот файл содержит следующие параметры:
*
* * Настройки MySQL
* * Секретные ключи
* * Префикс таблиц базы данных
* * ABSPATH
*
* @link https://codex.wordpress.org/Editing_wp-config.php
*
* @package WordPress
*/

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wp');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
* Уникальные ключи и соли для аутентификации.
*
* Смените значение каждой константы на уникальную фразу.
* Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
* Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
*
* @since 2.6.0
*/
define('AUTH_KEY', 'dvxHkKVzeXyyfYg5cygiB9+sWjjVIpxm2lx2J32HaqONcr+ce1MHPR2a+JXt4IQs');
define('SECURE_AUTH_KEY', 'oIHxvlNLj+Ju9RyyI4k3xDrqJVL4+i/6z/q3h+8jv2k32r3BBBA+nhQumtSe8MnV');
define('LOGGED_IN_KEY', 'BTgkSy9fjRCp73YUivuWMUI4Rw7BX3t31NrljQ7dR0D2uQDZEATqpaVvvN2agWK/');
define('NONCE_KEY', 'X+fz1TTT0EydTIAqrn+/EiNXoaOVnP9qOWawqff+EBYUZjxfLsTGXGOir91QU+J1');
define('AUTH_SALT', 'tNX5pJKNqanwlUfgg8tN1o6Daz7oS8KkSJQO4081TtwlEiRSfjcoSAgrCTJXnFnj');
define('SECURE_AUTH_SALT', 'U7VqsJB/ip0xLjKF/JIZGuOeL0Rc9KQJhQ2TJC9koV6+qXG72qrN4Jczhcpl4OXg');
define('LOGGED_IN_SALT', 'radvdhrMw/dkcpY8z/QzvgY6mkpI4O72ubCj6fSv4BM5x3NRVEnrFOsbeaKBCk3N');
define('NONCE_SALT', 'hNtKJZNlZb6oTMAeY+yfrmwurJysWMyl64qj2WQXX02d86qSQe3mjxWe9+TD67E0');

/**#@-*/

/**
* Префикс таблиц в базе данных WordPress.
*
* Можно установить несколько сайтов в одну базу данных, если использовать
* разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
*/
$table_prefix = 'wp_';

/**
* Для разработчиков: Режим отладки WordPress.
*
* Измените это значение на true, чтобы включить отображение уведомлений при разработке.
* Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
* в своём рабочем окружении.
*
* Информацию о других отладочных константах можно найти в Кодексе.
*
* @link https://codex.wordpress.org/Debugging_in_WordPress
*/
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
